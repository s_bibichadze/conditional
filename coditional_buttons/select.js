const select_ = () => {
    
    let button_field = document.getElementById('draw_condi');
    let create_btn = document.createElement('select');
    let div_all = document.createElement('div');
    let div_copy_conditional = document.createElement('div');
    let div_component = document.createElement('div');
    let conditional_button = document.createElement('button');
    let butt_rem = document.createElement('button');
    let option_1=document.createElement('option');
    let option_2=document.createElement('option');
    let option_3=document.createElement('option');
    let option_4=document.createElement('option');

    let conditional = document.createElement('div');
    let conditional_table1 = document.createElement('div');
    let conditional_table1_1 = document.createElement('p');
    let conditional_table1_2 = document.createElement('input');
    let conditional_table1_3 = document.createElement('select');
    let conditional_table1_4 = document.createElement('input');
    let conditional_table2 = document.createElement('div');
    let conditional_table2_1 = document.createElement('p');
    let conditional_table2_2 = document.createElement('input');
    let conditional_table2_3 = document.createElement('select');
    let conditional_table2_4 = document.createElement('input');

    conditional_table1.setAttribute('class', 'fiels-table');
    conditional_table2.setAttribute('class', 'fiels-table');

    conditional_table1_2.setAttribute('content', 'all-field');
    conditional_table1_2.setAttribute('class', 'all-field');
    conditional_table1_2.setAttribute('type', "text");
    conditional_table1_2.setAttribute("placeholder", "source")
    conditional_table1_2.textContent = 'input';

    conditional_table1_4.setAttribute('content', 'all-field');
    conditional_table1_4.setAttribute('class', 'all-field');
    conditional_table1_4.setAttribute('type', "text");
    conditional_table1_4.setAttribute("placeholder", "target/value")
    conditional_table1_4.textContent = 'input';

    conditional_table2_2.setAttribute('content', 'all-field');
    conditional_table2_2.setAttribute('class', 'all-field');
    conditional_table2_2.setAttribute('type', "text");
    conditional_table2_2.setAttribute("placeholder", "target")
    conditional_table2_2.textContent = 'input';

    conditional_table2_4.setAttribute('content', 'all-field');
    conditional_table2_4.setAttribute('class', 'all-field');
    conditional_table2_4.setAttribute('type', "text");
    conditional_table2_4.setAttribute("placeholder", "value")
    conditional_table2_4.textContent = 'input';

    conditional_table1_1.innerText = "IF";
    conditional_table2_1.innerText = "THEN";

    create_btn.setAttribute('content', 'all-field');
    create_btn.setAttribute('class', 'all-field');
    option_1.textContent="1";
    option_2.textContent="2";
    option_3.textContent="3";
    option_4.textContent="8";
    create_btn.appendChild(option_1);
    create_btn.appendChild(option_2);
    create_btn.appendChild(option_3);
    create_btn.appendChild(option_4);
    
    document.getElementById("draw_condi").style.border = "none";

    butt_rem.setAttribute('content', '');
    butt_rem.setAttribute('class', 'all-field');  
    butt_rem.textContent = 'X';


    div_all.setAttribute('class', 'fiels-div')


    conditional_button.setAttribute('content', '');
    conditional_button.setAttribute('class', 'all-field');  
    conditional_button.textContent = 'con';

    div_all.appendChild(div_copy_conditional);
    div_all.appendChild(div_component);
    div_all.appendChild(conditional);
    div_component.appendChild(create_btn);
    div_copy_conditional.appendChild(butt_rem);
    div_copy_conditional.appendChild(conditional_button);
    button_field.appendChild(div_all);

    butt_rem.onclick=function(){
        div_all.remove();
    }

    
    conditional.appendChild(conditional_table1);
    conditional_table1.appendChild(conditional_table1_1)
    conditional_table1.appendChild(conditional_table1_2)
    conditional_table1.appendChild(conditional_table1_3)
    conditional_table1.appendChild(conditional_table1_4)

    conditional_table2.appendChild(conditional_table2_1)
    conditional_table2.appendChild(conditional_table2_2)
    conditional_table2.appendChild(conditional_table2_3)
    conditional_table2.appendChild(conditional_table2_4)

    conditional.appendChild(conditional_table2);
    conditional.style.display = "none";

    conditional_button.onclick=function(){
 
        if (div_component.style.display === "none") {
            div_component.style.display = "block";
            conditional.style.display = "none";
          } else {
            div_component.style.display = "none";
            conditional.style.display = "block";
          }

    }
       
}
document.getElementById('select__').addEventListener('click', select_);

