const radio = () => {
    
    let radio = document.getElementById('draw_condi');
    let radio_text = document.createTextNode("Radio: ");
    let create_radio = document.createElement('input');
    
    let div_all = document.createElement('div');
    let div_copy_conditional = document.createElement('div');
    let div_component = document.createElement('div');
    let conditional_button = document.createElement('button');
    let butt_rem = document.createElement('button');

    let conditional = document.createElement('div');
    let conditional_table1 = document.createElement('div');
    let conditional_table1_1 = document.createElement('p');
    let conditional_table1_2 = document.createElement('input');
    let conditional_table1_3 = document.createElement('select');
    let conditional_table1_4 = document.createElement('input');
    let conditional_table2 = document.createElement('div');
    let conditional_table2_1 = document.createElement('p');
    let conditional_table2_2 = document.createElement('input');
    let conditional_table2_3 = document.createElement('select');
    let conditional_table2_4 = document.createElement('input');

    conditional_table1.setAttribute('class', 'fiels-table');
    conditional_table2.setAttribute('class', 'fiels-table');

    conditional_table1_2.setAttribute('content', 'all-field');
    conditional_table1_2.setAttribute('class', 'all-field');
    conditional_table1_2.setAttribute('type', "text");
    conditional_table1_2.setAttribute("placeholder", "source")
    conditional_table1_2.textContent = 'input';

    conditional_table1_4.setAttribute('content', 'all-field');
    conditional_table1_4.setAttribute('class', 'all-field');
    conditional_table1_4.setAttribute('type', "text");
    conditional_table1_4.setAttribute("placeholder", "target/value")
    conditional_table1_4.textContent = 'input';

    conditional_table2_2.setAttribute('content', 'all-field');
    conditional_table2_2.setAttribute('class', 'all-field');
    conditional_table2_2.setAttribute('type', "text");
    conditional_table2_2.setAttribute("placeholder", "target")
    conditional_table2_2.textContent = 'input';

    conditional_table2_4.setAttribute('content', 'all-field');
    conditional_table2_4.setAttribute('class', 'all-field');
    conditional_table2_4.setAttribute('type', "text");
    conditional_table2_4.setAttribute("placeholder", "value")
    conditional_table2_4.textContent = 'input';

    conditional_table1_1.innerText = "IF";
    conditional_table2_1.innerText = "THEN";
    

    create_radio.setAttribute("type", "radio");
    create_radio.setAttribute('content', '');
    create_radio.setAttribute('class', 'all-field'); 

    document.getElementById("draw_condi").style.border = "none";

    butt_rem.setAttribute('content', '');
    butt_rem.setAttribute('class', 'all-field');  
    butt_rem.textContent = 'X';


    div_all.setAttribute('class', 'fiels-div')

    conditional_button.setAttribute('content', '');
    conditional_button.setAttribute('class', 'all-field');  
    conditional_button.textContent = 'con';


    div_all.appendChild(div_copy_conditional);
    div_all.appendChild(div_component);
    div_all.appendChild(conditional);
    div_component.appendChild(radio_text);
    div_component.appendChild(create_radio);
    div_copy_conditional.appendChild(butt_rem);
    div_copy_conditional.appendChild(conditional_button);
    radio.appendChild(div_all);

    butt_rem.onclick=function(){
        div_all.remove();
    }
    
    conditional.appendChild(conditional_table1);
    conditional_table1.appendChild(conditional_table1_1)
    conditional_table1.appendChild(conditional_table1_2)
    conditional_table1.appendChild(conditional_table1_3)
    conditional_table1.appendChild(conditional_table1_4)

    conditional_table2.appendChild(conditional_table2_1)
    conditional_table2.appendChild(conditional_table2_2)
    conditional_table2.appendChild(conditional_table2_3)
    conditional_table2.appendChild(conditional_table2_4)

    conditional.appendChild(conditional_table2);
    conditional.style.display = "none";

    conditional_button.onclick=function(){
 
        if (div_component.style.display === "none") {
            div_component.style.display = "block";
            conditional.style.display = "none";
          } else {
            div_component.style.display = "none";
            conditional.style.display = "block";
          }

    }


}
document.getElementById('radio').addEventListener('click', radio);
